import { useClerk } from "@clerk/clerk-react";

export const SignOutButton = () => {
  const { signOut } = useClerk();
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  return <button onClick={() => signOut()}>Sign out</button>;
};
